# ![Tapbuy](https://drive.google.com/uc?id=0B4nM6cjFIhuRRTVDSnZHMW40TDg =200x)

# Tapbuy SDK Sample Application

Tapbuy lets customers buy products everywhere in a few taps. This sample app. is available to check how the Tapbuy SDK can be implemented.

**Conditions**: [Xcode 8](https://developer.apple.com/xcode/) and iOS 8 or later. Xcode 8 requires a Mac running macOS Sierra 10.12 or OS X El Capitan 10.11.5 or later.

## SDK Features
Tapbuy SDK for iOS is the easiest way to use Tapbuy on your iOS app : Sell your products through the Tapbuy express checkout solution.

Check the [Tapbuy documentation](https://docs.tapbuy.io/sdk/ios) for more details.

## Bugs & feedback
For more details, bug feedback, or support contact us at [support@tapbuy.io](mailto:support@tapbuy.io?Subject=sdk%20ios%20support)

## License

    Copyright (c) 2016. Tapbuy

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.