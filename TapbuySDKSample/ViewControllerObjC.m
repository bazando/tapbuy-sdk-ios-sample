// Copyright (c) 2016. Tapbuy (http://tapbuy.io)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#import <Foundation/Foundation.h>
#import "ViewControllerObjC.h"

@import TapbuySDK_iOS_swift;

@implementation ViewControllerObjC

- (void) doCheckoutProcess{
    
    // Build SDK global parameters
    TapbuySDKGlobalParameters *globalParams = [[TapbuySDKGlobalParameters alloc] init];
    globalParams.debug = YES;
    globalParams.sandbox = YES;
    
    // Apply SDK global parameters
    [[TapbuySDK sharedInstance] setParameters:globalParams];
    
    // Build custom parameters for checkout
    TapbuySDKCheckoutParameters *checkoutParams = [[TapbuySDKCheckoutParameters alloc] init];
    checkoutParams.userEmail = @"firstname.lastname@domain.com";
    checkoutParams.userAddress = @"1, infinite loop street";
    checkoutParams.userCountryIsoCode = @"fr";
    checkoutParams.displayOptions = @[@(TapbuySDKCheckoutDisplayOptionSimilarProducts), @(TapbuySDKCheckoutDisplayOptionComplementaryProducts)];
    
    // Display the checkout view controller for a given product URL and parameters
    [[TapbuySDK sharedInstance] checkout:[[NSURL alloc] initWithString:@"PRODUCT URL HERE"] parameters:checkoutParams viewController:nil];
    
}

@end
