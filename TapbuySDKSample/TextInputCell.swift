// Copyright (c) 2016. Tapbuy (http://tapbuy.io)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import Foundation
import UIKit

class TextInputCell: ValueCell, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textInput: UITextField!
    
    var pickerView: UIPickerView?
    var pickerLabels: [String]?
    var pickerValues: [String]?
    
    func setData(label: String, value: String? = "") {
        
        self.titleLabel.text = label
        self.textInput.text = value
        self.textInput.placeholder = label
        self.pickerLabels = nil
        self.pickerValues = nil
        
        self.textInput.addTarget(self, action: #selector(notifyDelegate), for: .editingChanged)
        
        // Reset picker because of reusable cells
        
        self.textInput.inputView = nil
        
        if let gestures = self.pickerView?.gestureRecognizers {
            for gesture in gestures {
                gesture.delegate = nil
                self.pickerView?.removeGestureRecognizer(gesture)
            }
        }
        self.pickerView = nil
        
    }
    
    func setPickerData(labels: [String], values: [String]) {
        
        self.pickerLabels = labels
        self.pickerValues = values
        
        let picker = UIPickerView()
        picker.delegate = self
        self.textInput.inputView = picker
        
        // Select value, this is not done automatically
        let v = self.textInput.text ?? ""
        if v.characters.count > 0 {
            if let i = values.index(of: v) {
                picker.selectRow(i, inComponent: 0, animated: false)
            }
        }
        
        // Allow tap on the picker
        let gesture = UITapGestureRecognizer(target: self, action: #selector(pickerTapped))
        picker.addGestureRecognizer(gesture)
        gesture.delegate = self
        
        self.pickerView = picker
        
    }
    
    func getValue() -> String {
        
        return self.textInput.text ?? ""
        
    }
    
    override func click() {
        
        self.textInput.becomeFirstResponder()
        
    }
    
    // MARK: UIPickerView
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return self.pickerLabels?.count ?? 0
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return self.pickerLabels?[row]
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        self.textInput.text = self.pickerValues?[row]
        self.textInput.resignFirstResponder()
        
    }
    
    // MARK: UIGestureRecognizerDelegate
    
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        
        return true
        
    }
    
    func pickerTapped() {
        
        if let picker = self.pickerView {
            self.pickerView(picker, didSelectRow: picker.selectedRow(inComponent: 0), inComponent: 0)
        }
        
    }
    
}
