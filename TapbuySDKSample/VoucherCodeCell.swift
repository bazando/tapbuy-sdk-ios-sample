// Copyright (c) 2016. Tapbuy (http://tapbuy.io)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import Foundation
import UIKit

class VoucherCodeCell: ValueCell {
    
    @IBOutlet weak var textfield: UITextField!
    
    func setData(voucherCode: String) {
        
        self.textfield.text = voucherCode
        
        self.textfield.addTarget(self, action: #selector(notifyDelegate), for: .editingChanged)
        
    }
    
    override func click() {
        
        self.textfield.becomeFirstResponder()
        
    }
    
    func getValue() -> String {
        
        return self.textfield.text ?? ""
        
    }
    
    @IBAction func deleteButtonClicked(_ sender: AnyObject) {
        
        self.notifyDelegateForDeletion()
        
    }
    
}
