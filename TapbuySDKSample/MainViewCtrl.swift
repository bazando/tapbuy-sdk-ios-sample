// Copyright (c) 2016. Tapbuy (http://tapbuy.io)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import UIKit
import TapbuySDK_iOS_swift

class MainViewCtrl: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var checkoutParams: TapbuySDKCheckoutParameters
    
    // MARK: Controller life cycle
    
    required init?(coder aDecoder: NSCoder) {
        
        self.checkoutParams = TapbuySDKCheckoutParameters()
        
        super.init(coder: aDecoder)
        
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.title = "TapbuySDK checkout demo"
        
        // Setup UITableView
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "menuCell")
        
        // Display test and debug button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "optionsIcon")!, style: .plain, target: self, action: #selector(testAndDebugClicked))
        
    }
    
    func testAndDebugClicked() {
        
        // Display test and debug screen
        
        guard
            let ctrl = self.storyboard?.instantiateViewController(withIdentifier: "testAndDebugViewCtrl") as? TestAndDebugViewCtrl,
            let navCtrl = self.navigationController
        else { return }
        
        navCtrl.pushViewController(ctrl, animated: true)
        
    }
    
    // MARK: UITableView
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        // Return the number of menu items
        return 8
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Setup menu item titles
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath)
        
        switch (indexPath as NSIndexPath).row {
            
        case 1:
            cell.textLabel?.text = "Product with combination"
            break
            
        case 2:
            cell.textLabel?.text = "Skip product page"
            break
            
        case 3:
            cell.textLabel?.text = "Fill user registration info"
            break
            
        case 4:
            cell.textLabel?.text = "Display similar products"
            break
            
        case 5:
            cell.textLabel?.text = "Display complementary products"
            break
            
        case 6:
            cell.textLabel?.text = "Display similar and complementary products"
            break
            
        case 7:
            cell.textLabel?.text = "Apply voucher codes"
            break
            
        default:
            cell.textLabel?.text = "Default checkout"
            
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        // Handle menu item click
        // Setup checkout parameters according to the selected menu item
        
        var url = self.getDefaultProductURL()
        
        self.initDefaultCheckoutParams()
        
        switch (indexPath as NSIndexPath).row {
            
        case 1:
            url = self.getProductWithCombinationURL()
            break
            
        case 2:
            url = self.getProductWithCombinationURL()
            self.setSkipViewProduct()
            break
            
        case 3:
            self.setUserPrefillInfo()
            self.setShippingPrefillInfo()
            break
            
        case 4:
            self.setDisplaySimilarProducts()
            break
            
        case 5:
            self.setDisplayComplementaryProducts()
            break
            
        case 6:
            self.setDisplaySimilarProducts()
            self.setDisplayComplementaryProducts()
            break
            
        case 7:
            self.setVoucherCodes()
            break
            
        default: break
            
        }
        
        self.displayCheckoutController(url)
        
    }
    
    // MARK: Checkout parameters
    
    fileprivate func getDefaultProductURL() -> URL {
        
        // Put here any product URL without combination
        return URL(string: "https://thekooplesstaging.tapbuy.io/checkout/105")!
        
    }
    
    fileprivate func getProductWithCombinationURL() -> URL {
        
        // Put here any product URL with combination
        return URL(string: "https://thekooplesstaging.tapbuy.io/checkout/105/620")!
        
    }
    
    fileprivate func initDefaultCheckoutParams() {
        
        // Reset checkout parameters
        self.checkoutParams = TapbuySDKCheckoutParameters()
        
    }
    
    fileprivate func setSkipViewProduct() {
        
        // Skip the product page if possible
        self.checkoutParams.skipViewProduct = true
        
    }
    
    fileprivate func setUserPrefillInfo() {
        
        // Pre-fill user information in registration form
        self.checkoutParams.userEmail = "elon@teslamotors.com"
        self.checkoutParams.userFirstName = "elon"
        self.checkoutParams.userLastName = "musk"
        self.checkoutParams.userAddress = "1, infinite loop street"
        self.checkoutParams.userAddress2 = "2nd floor"
        self.checkoutParams.userZipcode = "75000"
        self.checkoutParams.userCity = "Paris"
        self.checkoutParams.userCountryIsoCode = "fr"
        self.checkoutParams.userPhoneNumber = "0623456789"
        
    }
    
    fileprivate func setShippingPrefillInfo() {
        
        // Pre-fill shipping information in registration form
        self.checkoutParams.shippingFirstName = "john"
        self.checkoutParams.shippingLastName = "smith"
        self.checkoutParams.shippingAddress = "2, forest ave"
        self.checkoutParams.shippingAddress2 = "door 413"
        self.checkoutParams.shippingZipcode = "33000"
        self.checkoutParams.shippingCity = "Bordeaux"
        self.checkoutParams.shippingCountryIsoCode = "fr"
        self.checkoutParams.shippingPhoneNumber = "0612345678"
        
    }

    
    fileprivate func setDisplaySimilarProducts() {
        
        // Display products similar to the product to buy
        if !self.checkoutParams.displayOptions.contains(TapbuySDKCheckoutDisplayOption.similarProducts.rawValue) {
            let tmp = NSMutableArray(array: self.checkoutParams.displayOptions)
            tmp.add(TapbuySDKCheckoutDisplayOption.similarProducts.rawValue)
            self.checkoutParams.displayOptions = tmp
        }
        
    }
    
    fileprivate func setDisplayComplementaryProducts() {
        
        // Display products complementary to the product to buy
        if !self.checkoutParams.displayOptions.contains(TapbuySDKCheckoutDisplayOption.complementaryProducts.rawValue) {
            let tmp = NSMutableArray(array: self.checkoutParams.displayOptions)
            tmp.add(TapbuySDKCheckoutDisplayOption.complementaryProducts.rawValue)
            self.checkoutParams.displayOptions = tmp
        }
        
    }
    
    fileprivate func setVoucherCodes() {
        
        // Apply voucher codes
        self.checkoutParams.voucherCodes.append("TAPBUY")
        self.checkoutParams.voucherCodes.append("DELIVERY")
        
    }
    
    // MARK: Checkout itself
    
    fileprivate func displayCheckoutController(_ productURL: URL) {
        
        // Build SDK global parameters
        let globalParams = TapbuySDKGlobalParameters()
        globalParams.debug = true
        globalParams.sandbox = true
        
        // Apply SDK global parameters
        TapbuySDK.sharedInstance().setParameters(globalParams)
        
        // Display the checkout view controller for the given product URL and parameters
        TapbuySDK.sharedInstance().checkout(productURL, parameters: self.checkoutParams, viewController: nil)
        
    }
    
}
