// Copyright (c) 2016. Tapbuy (http://tapbuy.io)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import Foundation
import UIKit

protocol TestAndDebugLoadSettingsDelegate {
    func load(key: String)
}

class TestAndDebugLoadSettingsViewCtrl: UITableViewController, SettingsCellDelegate {
    
    var delegate: TestAndDebugLoadSettingsDelegate?
    
    var keyList = [String]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.title = "Load settings"
        
        self.tableView.register(UINib(nibName: "SettingsCell", bundle: nil), forCellReuseIdentifier: "settingsCell")
        
        self.reload()
        
    }
    
    func reload() {
        
        self.keyList = Helper.listTNDKeys()
        
        self.tableView.reloadData()
        
    }
    
    // MARK: UITableView
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let count = self.keyList.count
        
        if count == 0 {
            
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.size.width, height: self.tableView.bounds.size.height))
            label.textAlignment = .center
            label.text = "No data for the moment"
            
            self.tableView.separatorStyle = .none
            self.tableView.backgroundView = label
            
        } else {
            
            self.tableView.separatorStyle = .singleLine
            self.tableView.backgroundView = nil
            
        }
        
        return count
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell", for: indexPath) as? SettingsCell {
            
            cell.setData(keyName: self.keyList[indexPath.row])
            cell.delegate = self
            
            return cell
            
        }
        
        // Should not occur
        return UITableViewCell()
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        
    }
    
    // MARK: SettingsCellDelegate
    
    func loadSettings(keyName: String) {
        
        self.delegate?.load(key: keyName)
        
        _ = self.navigationController?.popViewController(animated: true)
        
    }
    
    func deleteSettings(keyName: String) {
        
        Helper.removeTNDParameters(key: keyName)
        
        self.reload()
        
    }
    
}
