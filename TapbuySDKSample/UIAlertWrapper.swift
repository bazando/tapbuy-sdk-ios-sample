// Copyright (c) 2016. Tapbuy (http://tapbuy.io)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import Foundation
import UIKit

private enum UIAlertWrapperPresentationStyle {
    case Rect (CGRect)
    case BarButton (UIBarButtonItem)
}

struct UIAlertWrapperTextfieldTarget {
    var target: AnyObject?
    var selector: Selector
    var controlEvent: UIControlEvents
}

/**
 UIAlertWrapper is a wrapper around UIAlertView/UIActionSheet and UIAlertController in order to simplify supporting both iOS 7 and iOS 8. It is not meant to be an exhaustive wrapper, it merely covers the common use cases around Alert Views and Action Sheets.
 */
class UIAlertWrapper : NSObject, UIAlertViewDelegate, UIActionSheetDelegate {
    
    var presentationCompletionBlock: (() -> Void)?
    
    var clickedButtonAtIndexBlock:((Int) -> Void)?
    var clickedButtonAtIndexBlockForTexfield:((Int, String?) -> Void)?
    
    var withTextfield: Bool
    var textfieldValue: String?
    var textfieldPlaceholder: String?
    var textfieldTargets: [UIAlertWrapperTextfieldTarget]?
    
    override init() {
        
        self.withTextfield = false
        
        super.init()
        
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        
        if self.withTextfield {
            if let tf = alertView.textField(at: 0) as UITextField? {
                self.clickedButtonAtIndexBlockForTexfield?(buttonIndex, tf.text)
            }
        }
        
        self.clickedButtonAtIndexBlock?(buttonIndex)
        
    }
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int) {
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.clickedButtonAtIndexBlock?(buttonIndex + 1)
        } else {
            self.clickedButtonAtIndexBlock?(buttonIndex)
        }
        
    }
    
    /**
     Initializes and presents a new Action Sheet from a Bar Button Item on iPad or modally on iPhone
     
     - parameter title: The title of the Action Sheet
     - parameter cancelButtonTitle: The cancel button title
     - parameter destructiveButtonTitle: The destructive button title
     - parameter otherButtonTitles: An array of other button titles
     - parameter barButtonItem: The Bar Button Item to present from
     */
    func presentActionSheet(title: String?, cancelButtonTitle: String, destructiveButtonTitle: String?, otherButtonTitles: [String], barButtonItem:UIBarButtonItem) {
        
        self.presentActionSheet(
            title: title,
            cancelButtonTitle: cancelButtonTitle,
            destructiveButtonTitle: destructiveButtonTitle,
            otherButtonTitles: otherButtonTitles,
            presentationStyle: .BarButton(barButtonItem)
        )
        
    }
    
    /**
     Initializes and presents a new Action Sheet from a Frame on iPad or modally on iPhone
     
     - parameter title: The title of the Action Sheet
     - parameter cancelButtonTitle: The cancel button title
     - parameter destructiveButtonTitle: The destructive button title
     - parameter otherButtonTitles: An array of other button titles
     - parameter frame: The Frame to present from
     */
    func presentActionSheet(title: String?, cancelButtonTitle: String, destructiveButtonTitle:String?, otherButtonTitles: [String], frame:CGRect) {
        
        self.presentActionSheet(
            title: title,
            cancelButtonTitle: cancelButtonTitle,
            destructiveButtonTitle: destructiveButtonTitle,
            otherButtonTitles: otherButtonTitles,
            presentationStyle: .Rect(frame)
        )
        
    }
    
    private func presentActionSheet(title: String?, cancelButtonTitle: String, destructiveButtonTitle:String?, otherButtonTitles: [String], presentationStyle:UIAlertWrapperPresentationStyle) {
        
        guard
            let currentViewController = UIApplication.topViewController()
            else { return }
        
        if #available(iOS 8.0, *) {
            
            var buttonOffset = 1
            let alert = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
            
            alert.addAction(UIAlertAction(
                title: cancelButtonTitle,
                style: .cancel,
                handler: { (alert: UIAlertAction) in
                    self.clickedButtonAtIndexBlock?(0)
                }
            ))
            
            if let _destructiveButtonTitle = destructiveButtonTitle {
                alert.addAction(UIAlertAction(
                    title: _destructiveButtonTitle,
                    style: .destructive,
                    handler: { (alert: UIAlertAction) in
                        self.clickedButtonAtIndexBlock?(1)
                    }
                ))
                buttonOffset += 1
            }
            
            for (index, string) in otherButtonTitles.enumerated() {
                alert.addAction(UIAlertAction(
                    title: string,
                    style: .default,
                    handler: {(alert: UIAlertAction) in
                        self.clickedButtonAtIndexBlock?(index + buttonOffset)
                    }
                ))
            }
            
            if UIDevice.current.userInterfaceIdiom == .pad {
                switch presentationStyle {
                case let .Rect(rect):
                    alert.popoverPresentationController?.sourceView = currentViewController.view
                    alert.popoverPresentationController?.sourceRect = rect
                case let .BarButton(barButton):
                    alert.popoverPresentationController?.barButtonItem = barButton
                }
            }
            
            currentViewController.present(alert, animated: true, completion: self.presentationCompletionBlock)
            
        } else {
            
            let alert = UIActionSheet()
            
            if let _title = title {
                alert.title = _title
            }
            
            if UIDevice.current.userInterfaceIdiom == .phone {
                alert.cancelButtonIndex = 0
                alert.addButton(withTitle: cancelButtonTitle)
            }
            
            if let _destructiveButtonTitle = destructiveButtonTitle {
                alert.destructiveButtonIndex = UIDevice.current.userInterfaceIdiom == .phone ? 1 : 0
                alert.addButton(withTitle: _destructiveButtonTitle)
            }
            
            for string in otherButtonTitles {
                alert.addButton(withTitle: string)
            }
            
            alert.delegate = self
            
            if UIDevice.current.userInterfaceIdiom == .phone {
                if let tabBar = currentViewController.tabBarController?.tabBar {
                    alert.show(from: tabBar)
                } else {
                    alert.show(in: currentViewController.view)
                }
            } else {
                switch presentationStyle {
                case let .Rect(rect):
                    alert.show(from: rect, in: currentViewController.view, animated: true)
                case let .BarButton(barButton):
                    alert.show(from: barButton, animated: true)
                }
            }
        }
        
    }
    
    /**
     Initializes and presents a new Alert
     
     - parameter title: The title of the Alert
     - parameter message: The message of the Alert
     - parameter cancelButtonTitle: The cancel button title
     - parameter otherButtonTitles: An array of other button titles
     */
    func presentAlert(title: String, message: String, cancelButtonTitle: String, otherButtonTitles: [String]? = nil) {
        
        if #available(iOS 8.0, *) {
            
            if let currentViewController = UIApplication.topViewController() {
                
                var tf: UITextField?
                
                let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
                
                if self.withTextfield {
                    alert.addTextField(configurationHandler: { (textField: UITextField) -> Void in
                        textField.text = self.textfieldValue
                        textField.placeholder = self.textfieldPlaceholder
                        if let targets = self.textfieldTargets {
                            for t in targets {
                                textField.addTarget(t.target, action: t.selector, for: t.controlEvent)
                            }
                        }
                        tf = textField
                    })
                }
                
                alert.addAction(UIAlertAction(
                    title: cancelButtonTitle,
                    style: .cancel,
                    handler: { (alert: UIAlertAction) in
                        if let text = tf?.text {
                            self.clickedButtonAtIndexBlockForTexfield?(0, text)
                        }
                        self.clickedButtonAtIndexBlock?(0)
                    }
                ))
                
                if let _otherButtonTitles = otherButtonTitles {
                    for (index, string) in _otherButtonTitles.enumerated() {
                        alert.addAction(UIAlertAction(
                            title: string,
                            style: .default,
                            handler: { (alert: UIAlertAction) in
                                if let text = tf?.text {
                                    self.clickedButtonAtIndexBlockForTexfield?(index + 1, text)
                                }
                                self.clickedButtonAtIndexBlock?(index + 1)
                            }
                        ))
                    }
                }
                
                currentViewController.present(alert, animated: true, completion: self.presentationCompletionBlock)
                
            }
            
            
        } else {
            
            let alert = UIAlertView()
            alert.addButton(withTitle: cancelButtonTitle);
            alert.cancelButtonIndex = 0
            alert.title = title
            alert.message = message
            
            if let _otherButtonTitles = otherButtonTitles {
                for string in _otherButtonTitles {
                    alert.addButton(withTitle: string)
                }
            }
            
            if self.withTextfield {
                alert.alertViewStyle = .plainTextInput
            }
            
            alert.delegate = self
            
            alert.show()
            
        }
        
    }
    
    func didPresent(_ alertView: UIAlertView) {
        self.presentationCompletionBlock?()
    }
    
    func didPresent(_ actionSheet: UIActionSheet) {
        self.presentationCompletionBlock?()
    }
    
}
