// Copyright (c) 2016. Tapbuy (http://tapbuy.io)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import Foundation
import UIKit
import TapbuySDK_iOS_swift

class Helper {
    
    /// Get Tapbuy button color
    static func getTapbuyButtonColor() -> UIColor {
        
        return UIColor(red: 2/255, green: 193/255, blue: 154/255, alpha: 1)
        
    }
    
    /// Save Test And debug parameters
    static func saveTNDParameters(key: String, productURL: URL?, globalParameters: TapbuySDKGlobalParameters, checkoutParameters: TapbuySDKCheckoutParameters) {
        
        var data = [String: Any]()
        if let tmp = productURL {
            data["productURL"] = tmp
        }
        data["globalParameters"] = globalParameters
        data["checkoutParameters"] = checkoutParameters
        
        let defaults = UserDefaults.standard
        defaults.setValue(NSKeyedArchiver.archivedData(withRootObject: data), forKey: "TapbuySDKSample_" + key)
        defaults.synchronize()
        
    }
    
    /// Load Test And debug parameters
    static func loadTNDParameters(key: String) -> [String: Any]? {
        
        guard
            let tmpData = UserDefaults.standard.value(forKey: "TapbuySDKSample_" + key) as? Data,
            let data = NSKeyedUnarchiver.unarchiveObject(with: tmpData) as? [String: Any]
        else { return nil }
        
        return data
        
    }
    
    /// Remove Test And debug parameters
    static func removeTNDParameters(key: String) {
        
        let defaults = UserDefaults.standard
        
        if let _ = defaults.value(forKey: "TapbuySDKSample_" + key) {
            
            defaults.removeObject(forKey: "TapbuySDKSample_" + key)
            defaults.synchronize()
            
        }
        
    }
    
    /// List Test And debug parameter keys
    static func listTNDKeys() -> [String] {
        
        var result = [String]()
        
        let keys = UserDefaults.standard.dictionaryRepresentation().keys
        
        for key in keys {
            
            if key.indexOf(target: "TapbuySDKSample_") == 0 {
                
                result.append(key.description.substring(from: 16))
                
            }
            
        }
        
        return result
        
    }
    
}
