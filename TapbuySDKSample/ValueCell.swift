// Copyright (c) 2016. Tapbuy (http://tapbuy.io)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import Foundation
import UIKit

@objc protocol ValueCellDelegate {
    func cellValueUpdated(updatedCell: UITableViewCell, indexPath: IndexPath)
    @objc optional func cellValueDeleted(updatedCell: UITableViewCell, indexPath: IndexPath)
}

class ValueCell: UITableViewCell {
    
    var indexPath: IndexPath?
    
    var delegate: ValueCellDelegate?
    
    func click() {
        // To be overriden
    }
    
    func notifyDelegate() {
        
        guard
            let d = self.delegate,
            let path = self.indexPath
        else { return }
        
        d.cellValueUpdated(updatedCell: self, indexPath: path)
        
    }
    
    func notifyDelegateForDeletion() {
        
        guard
            let d = self.delegate,
            let path = self.indexPath
        else { return }
        
        d.cellValueDeleted?(updatedCell: self, indexPath: path)
        
    }
    
}
