// Copyright (c) 2016. Tapbuy (http://tapbuy.io)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import Foundation
import UIKit

class OnOffCell: ValueCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var stateSwitch: UISwitch!
    
    func setData(label: String, value: Bool = false) {
        
        self.titleLabel.text = label
        self.stateSwitch.isOn = value
        
        self.stateSwitch.addTarget(self, action: #selector(notifyDelegate), for: .valueChanged)
        
    }
    
    func getValue() -> Bool {
        
        return self.stateSwitch.isOn
        
    }
    
    override func click() {
        
        self.stateSwitch.isOn = !self.stateSwitch.isOn
        self.notifyDelegate()
        
    }
    
}
