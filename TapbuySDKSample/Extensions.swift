// Copyright (c) 2016. Tapbuy (http://tapbuy.io)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import Foundation

// MARK: Dictionary

// "Concatenate" dictionaries
func +=<K, V> (left: inout [K : V], right: [K : V]) { for (k, v) in right { left[k] = v } }

// MARK: String

extension String {
    
    var length: Int {
        get {
            return self.characters.count
        }
    }
    
    func getPrepended(s: String) -> String {
        return s + self
    }
    
    mutating func prepend(s: String) {
        
        self = self.getPrepended(s: s)
        
    }
    
    func indexOf(target: String) -> Int {
        
        if let range = self.range(of: target) {
            return self.distance(from: self.startIndex, to: range.lowerBound)
        } else {
            return -1
        }
        
    }
    
    public func indexOfCharacter(char: Character) -> Int? {
        if let idx = self.characters.index(of: char) {
            return self.characters.distance(from: startIndex, to: idx)
        }
        return nil
    }
    
    func indexOf(target: String, startIndex: Int) -> Int {
        
        let startRange = self.index(self.startIndex, offsetBy: startIndex)
        
        if let range = self.range(of: target, options: NSString.CompareOptions.literal, range: startRange..<self.endIndex) {
            return self.distance(from: self.startIndex, to: range.lowerBound)
        } else {
            return -1
        }
        
    }
    
    func lastIndexOf(target: String) -> Int {
        
        var index = -1
        var stepIndex = self.indexOf(target: target)
        while stepIndex > -1 {
            
            index = stepIndex
            if stepIndex + target.length < self.length {
                stepIndex = indexOf(target: target, startIndex: stepIndex + target.length)
            } else {
                stepIndex = -1
            }
            
        }
        
        return index
        
    }
    
    func index(from: Int) -> Index {
        
        return self.index(self.startIndex, offsetBy: from)
        
    }
    
    func substring(from: Int) -> String {
        
        let fromIndex = self.index(from: from)
        
        return substring(from: fromIndex)
        
    }
    
    func substring(to: Int) -> String {
        
        let toIndex = self.index(from: to)
        
        return substring(to: toIndex)
        
    }
    
    func substring(with r: Range<Int>) -> String {
        
        let startIndex = self.index(from: r.lowerBound)
        let endIndex = self.index(from: r.upperBound)
        
        return self.substring(with: startIndex..<endIndex)
        
    }
    
    func getLeftTrimmed(toRemove: String) -> String {
        
        let index = self.indexOf(target: toRemove)
        
        if index == 0 {
            
            return self.substring(from: toRemove.length)
            
        }
        
        return self
        
    }
    
    mutating func leftTrim(toRemove: String) {
        self = self.getLeftTrimmed(toRemove: toRemove)
    }
    
    func getRightTrimmed(toRemove: String) -> String {
        
        let index = self.lastIndexOf(target: toRemove)
        
        if index == self.length - toRemove.length {
            
            return self.substring(to: self.length - toRemove.length)
            
        }
        
        return self
        
    }
    
    mutating func rightTrim(toRemove: String) {
        self = self.getRightTrimmed(toRemove: toRemove)
    }
    
}
