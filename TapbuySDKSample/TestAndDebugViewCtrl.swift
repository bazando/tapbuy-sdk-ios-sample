// Copyright (c) 2016. Tapbuy (http://tapbuy.io)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import Foundation
import UIKit
import TapbuySDK_iOS_swift

class TestAndDebugViewCtrl: UITableViewController, ButtonCellDelegate, ValueCellDelegate, VoucherAddCellDelegate, TestAndDebugLoadSettingsDelegate {
    
    var productURL: URL?
    var globalParameters = TapbuySDKGlobalParameters()
    var checkoutParameters = TapbuySDKCheckoutParameters()
    
    var loadedSettingsKey: String?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.title = "Test and debug"
        
        self.tableView.register(UINib(nibName: "TextInputCell", bundle: nil), forCellReuseIdentifier: "textInputCell")
        self.tableView.register(UINib(nibName: "VoucherAddCell", bundle: nil), forCellReuseIdentifier: "voucherAddCell")
        self.tableView.register(UINib(nibName: "VoucherCodeCell", bundle: nil), forCellReuseIdentifier: "voucherCodeCell")
        self.tableView.register(UINib(nibName: "OnOffCell", bundle: nil), forCellReuseIdentifier: "onOffCell")
        self.tableView.register(UINib(nibName: "ButtonCell", bundle: nil), forCellReuseIdentifier: "buttonCell")
        
        
        // Display test and debug button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "settingsIcon")!, style: .plain, target: self, action: #selector(settingsClicked))
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        self.tableView.reloadData()
        
    }
    
    // MARK: UITableView
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 7
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
        
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return section == 6 ? 0 : 50
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
            
        case 0:
            // Main data
            return 2
            
        case 1:
            // Billing address
            return 8
            
        case 2:
            // Shipping address
            return 8
            
        case 3:
            // Vouchers
            return self.checkoutParameters.voucherCodes.count + 1
            
        case 4:
            // Display options
            return 2
            
        case 5:
            // Other options
            return 3
            
        case 6:
            // Button
            return 1
            
        default:
            return 0
        }
        
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        switch section {
        
        case 0:
            return "Main data"
            
        case 1:
            return "Billing address"
            
        case 2:
            return "Shipping address"
            
        case 3:
            return "Vouchers"
            
        case 4:
            return "Display options"
            
        case 5:
            return "Other options"
            
        case 6:
            return nil
            
        default:
            return nil
        }
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        if let cell = tableView.cellForRow(at: indexPath) as? ValueCell {
            cell.click()
        }
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var result = UITableViewCell()
        
        switch indexPath.section {
            
        case 0:
            
            // Main data
            
            switch indexPath.row {
            
            case 0:
                if let cell = tableView.dequeueReusableCell(withIdentifier: "textInputCell", for: indexPath) as? TextInputCell {
                    cell.setData(label: "Product URL", value: self.productURL?.absoluteString)
                    result = cell
                }
                break
                
            case 1:
                if let cell = tableView.dequeueReusableCell(withIdentifier: "textInputCell", for: indexPath) as? TextInputCell {
                    cell.setData(label: "User email", value: self.checkoutParameters.userEmail)
                    result = cell
                }
                break
                
            default:
                break
                
            }
            
        case 1:
            
            // Billing address
            
            switch indexPath.row {
                
            case 0:
                if let cell = tableView.dequeueReusableCell(withIdentifier: "textInputCell", for: indexPath) as? TextInputCell {
                    cell.setData(label: "Firstname", value: self.checkoutParameters.userFirstName)
                    result = cell
                }
                break
                
            case 1:
                if let cell = tableView.dequeueReusableCell(withIdentifier: "textInputCell", for: indexPath) as? TextInputCell {
                    cell.setData(label: "Lastname", value: self.checkoutParameters.userLastName)
                    result = cell
                }
                break
                
            case 2:
                if let cell = tableView.dequeueReusableCell(withIdentifier: "textInputCell", for: indexPath) as? TextInputCell {
                    cell.setData(label: "Line 1", value: self.checkoutParameters.userAddress)
                    result = cell
                }
                break
                
            case 3:
                if let cell = tableView.dequeueReusableCell(withIdentifier: "textInputCell", for: indexPath) as? TextInputCell {
                    cell.setData(label: "Line 2", value: self.checkoutParameters.userAddress2)
                    result = cell
                }
                break
                
            case 4:
                if let cell = tableView.dequeueReusableCell(withIdentifier: "textInputCell", for: indexPath) as? TextInputCell {
                    cell.setData(label: "ZIP code", value: self.checkoutParameters.userZipcode)
                    result = cell
                }
                break
                
            case 5:
                if let cell = tableView.dequeueReusableCell(withIdentifier: "textInputCell", for: indexPath) as? TextInputCell {
                    cell.setData(label: "City", value: self.checkoutParameters.userCity)
                    result = cell
                }
                break
                
            case 6:
                if let cell = tableView.dequeueReusableCell(withIdentifier: "textInputCell", for: indexPath) as? TextInputCell {
                    cell.setData(label: "Country", value: self.checkoutParameters.userCountryIsoCode)
                    cell.setPickerData(labels: ["France"], values: ["FRA"])
                    result = cell
                }
                break
                
            case 7:
                if let cell = tableView.dequeueReusableCell(withIdentifier: "textInputCell", for: indexPath) as? TextInputCell {
                    cell.setData(label: "Phone", value: self.checkoutParameters.userPhoneNumber)
                    result = cell
                }
                break
                
            default:
                break
                
            }
            
        case 2:
            
            // Shipping address
            
            switch indexPath.row {
                
            case 0:
                if let cell = tableView.dequeueReusableCell(withIdentifier: "textInputCell", for: indexPath) as? TextInputCell {
                    cell.setData(label: "Firstname", value: self.checkoutParameters.shippingFirstName)
                    result = cell
                }
                break
                
            case 1:
                if let cell = tableView.dequeueReusableCell(withIdentifier: "textInputCell", for: indexPath) as? TextInputCell {
                    cell.setData(label: "Lastname", value: self.checkoutParameters.shippingLastName)
                    result = cell
                }
                break
                
            case 2:
                if let cell = tableView.dequeueReusableCell(withIdentifier: "textInputCell", for: indexPath) as? TextInputCell {
                    cell.setData(label: "Line 1", value: self.checkoutParameters.shippingAddress)
                    result = cell
                }
                break
                
            case 3:
                if let cell = tableView.dequeueReusableCell(withIdentifier: "textInputCell", for: indexPath) as? TextInputCell {
                    cell.setData(label: "Line 2", value: self.checkoutParameters.shippingAddress2)
                    result = cell
                }
                break
                
            case 4:
                if let cell = tableView.dequeueReusableCell(withIdentifier: "textInputCell", for: indexPath) as? TextInputCell {
                    cell.setData(label: "ZIP code", value: self.checkoutParameters.shippingZipcode)
                    result = cell
                }
                break
                
            case 5:
                if let cell = tableView.dequeueReusableCell(withIdentifier: "textInputCell", for: indexPath) as? TextInputCell {
                    cell.setData(label: "City", value: self.checkoutParameters.shippingCity)
                    result = cell
                }
                break
                
            case 6:
                if let cell = tableView.dequeueReusableCell(withIdentifier: "textInputCell", for: indexPath) as? TextInputCell {
                    cell.setData(label: "Country", value: self.checkoutParameters.shippingCountryIsoCode)
                    cell.setPickerData(labels: ["France"], values: ["FRA"])
                    result = cell
                }
                break
                
            case 7:
                if let cell = tableView.dequeueReusableCell(withIdentifier: "textInputCell", for: indexPath) as? TextInputCell {
                    cell.setData(label: "Phone", value: self.checkoutParameters.shippingPhoneNumber)
                    result = cell
                }
                break
                
                
            default:
                break
                
            }
            
        case 3:
            
            // Vouchers
            
            if indexPath.row == self.checkoutParameters.voucherCodes.count {
                
                // Display "add" cell below voucher code list
                
                if let cell = tableView.dequeueReusableCell(withIdentifier: "voucherAddCell", for: indexPath) as? VoucherAddCell {
                    cell.delegate = self
                    result = cell
                }
                
            } else {
                
                if let cell = tableView.dequeueReusableCell(withIdentifier: "voucherCodeCell", for: indexPath) as? VoucherCodeCell {
                    cell.setData(voucherCode: self.checkoutParameters.voucherCodes[indexPath.row])
                    cell.delegate = self
                    result = cell
                }
                
            }
            
            break
            
        case 4:
            
            // Display options
            
            switch indexPath.row {
                
            case 0:
                if let cell = tableView.dequeueReusableCell(withIdentifier: "onOffCell", for: indexPath) as? OnOffCell {
                    cell.setData(label: "Complementary products", value: self.checkoutParameters.displayOptions.contains(TapbuySDKCheckoutDisplayOption.complementaryProducts.rawValue))
                    result = cell
                }
                break
                
            case 1:
                if let cell = tableView.dequeueReusableCell(withIdentifier: "onOffCell", for: indexPath) as? OnOffCell {
                    cell.setData(label: "Similar products", value: self.checkoutParameters.displayOptions.contains(TapbuySDKCheckoutDisplayOption.similarProducts.rawValue))
                    result = cell
                }
                break
                
            default:
                break
                
            }
            
        case 5:
            
            // Other options
            
            switch indexPath.row {
                
            case 0:
                if let cell = tableView.dequeueReusableCell(withIdentifier: "onOffCell", for: indexPath) as? OnOffCell {
                    cell.setData(label: "Skip view product", value: self.checkoutParameters.skipViewProduct)
                    result = cell
                }
                break
                
            case 1:
                if let cell = tableView.dequeueReusableCell(withIdentifier: "onOffCell", for: indexPath) as? OnOffCell {
                    cell.setData(label: "Sandbox mode", value: self.globalParameters.sandbox)
                    result = cell
                }
                break
                
            case 2:
                if let cell = tableView.dequeueReusableCell(withIdentifier: "onOffCell", for: indexPath) as? OnOffCell {
                    cell.setData(label: "Debug mode", value: self.globalParameters.debug)
                    result = cell
                }
                break
                
            default:
                break
                
            }
            
        case 6:
            
            // Button
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: "buttonCell", for: indexPath) as? ButtonCell {
                cell.setData(buttonText: "RUN", delegate: self)
                result = cell
            }
            break
        
        default:
            break
            
        }
        
        if let c = result as? ValueCell {
            c.indexPath = indexPath
            c.delegate = self
        }
        
        return result
        
    }
    
    // MARK: ButtonCellDelegate
    
    func buttonClicked(button: UIButton) {
        
        if let url = self.productURL {
        
            TapbuySDK.sharedInstance().setParameters(globalParameters)
            
            TapbuySDK.sharedInstance().checkout(url, parameters: checkoutParameters, viewController: nil)
            
        } else {
            
            let alert = UIAlertWrapper()
            alert.presentAlert(title: "Error", message: "Please enter a product URL", cancelButtonTitle: "Close")
            
        }
        
    }
    
    // MARK: VoucherAddCellDelegate
    
    func addNewVoucher() {
        
        // Ask user the value of the new voucher code
        
        let textAlert = UIAlertWrapper()
        textAlert.withTextfield = true
        
        textAlert.clickedButtonAtIndexBlockForTexfield = { (buttonIndex, textValue) -> () in
            
            guard
                let value = textValue, value.length > 0 && buttonIndex == 1
            else { return }
            
            var alreadyPresent = false
            for voucher in self.checkoutParameters.voucherCodes {
                if voucher == value {
                    alreadyPresent = true
                    break
                }
            }
            
            if !alreadyPresent {
                self.checkoutParameters.voucherCodes.append(value)
                self.tableView.reloadData()
            }
            
        }
        
        textAlert.presentAlert(
            title: "Enter new voucher",
            message: "",
            cancelButtonTitle: "Cancel",
            otherButtonTitles: ["Ok"]
        )
        
    }
    
    // MARK: ValueCellDelegate
    
    func cellValueUpdated(updatedCell: UITableViewCell, indexPath: IndexPath) {
        
        switch indexPath.section {
            
        case 0:
            
            // Main data
            
            switch indexPath.row {
                
            case 0:
                self.productURL = URL(string: (updatedCell as! TextInputCell).getValue())
                break
                
            case 1:
                self.checkoutParameters.userEmail = (updatedCell as! TextInputCell).getValue()
                break
                
            default:
                break
                
            }
            
        case 1:
            
            // Billing address
            
            switch indexPath.row {
                
            case 0:
                self.checkoutParameters.userFirstName = (updatedCell as! TextInputCell).getValue()
                break
                
            case 1:
                self.checkoutParameters.userLastName = (updatedCell as! TextInputCell).getValue()
                break
                
            case 2:
                self.checkoutParameters.userAddress = (updatedCell as! TextInputCell).getValue()
                break
                
            case 3:
                self.checkoutParameters.userAddress2 = (updatedCell as! TextInputCell).getValue()
                break
                
            case 4:
                self.checkoutParameters.userZipcode = (updatedCell as! TextInputCell).getValue()
                break
                
            case 5:
                self.checkoutParameters.userCity = (updatedCell as! TextInputCell).getValue()
                break
                
            case 6:
                self.checkoutParameters.userCountryIsoCode = (updatedCell as! TextInputCell).getValue()
                break
                
            case 7:
                self.checkoutParameters.userPhoneNumber = (updatedCell as! TextInputCell).getValue()
                break
                
            default:
                break
                
            }
            
        case 2:
            
            // Shipping address
            
            switch indexPath.row {
                
            case 0:
                self.checkoutParameters.shippingFirstName = (updatedCell as! TextInputCell).getValue()
                break
                
            case 1:
                self.checkoutParameters.shippingLastName = (updatedCell as! TextInputCell).getValue()
                break
                
            case 2:
                self.checkoutParameters.shippingAddress = (updatedCell as! TextInputCell).getValue()
                break
                
            case 3:
                self.checkoutParameters.shippingAddress2 = (updatedCell as! TextInputCell).getValue()
                break
                
            case 4:
                self.checkoutParameters.shippingZipcode = (updatedCell as! TextInputCell).getValue()
                break
                
            case 5:
                self.checkoutParameters.shippingCity = (updatedCell as! TextInputCell).getValue()
                break
                
            case 6:
                self.checkoutParameters.shippingCountryIsoCode = (updatedCell as! TextInputCell).getValue()
                break
                
            case 7:
                self.checkoutParameters.shippingPhoneNumber = (updatedCell as! TextInputCell).getValue()
                break
                
                
            default:
                break
                
            }
            
        case 3:
            
            // Vouchers
            
            self.checkoutParameters.voucherCodes[indexPath.row] = (updatedCell as! VoucherCodeCell).getValue()
            
            break
            
        case 4:
            
            // Display options
            
            switch indexPath.row {
                
            case 0:
                if (updatedCell as! OnOffCell).getValue() == true {
                    if !self.checkoutParameters.displayOptions.contains(TapbuySDKCheckoutDisplayOption.complementaryProducts.rawValue) {
                        let tmp = NSMutableArray(array: self.checkoutParameters.displayOptions)
                        tmp.add(TapbuySDKCheckoutDisplayOption.complementaryProducts.rawValue)
                        self.checkoutParameters.displayOptions = tmp
                    }
                } else {
                    if self.checkoutParameters.displayOptions.contains(TapbuySDKCheckoutDisplayOption.complementaryProducts.rawValue) {
                        let tmp = NSMutableArray(array: self.checkoutParameters.displayOptions)
                        tmp.remove(TapbuySDKCheckoutDisplayOption.complementaryProducts.rawValue)
                        self.checkoutParameters.displayOptions = tmp
                    }
                }
                break
                
            case 1:
                if (updatedCell as! OnOffCell).getValue() == true {
                    if !self.checkoutParameters.displayOptions.contains(TapbuySDKCheckoutDisplayOption.similarProducts.rawValue) {
                        let tmp = NSMutableArray(array: self.checkoutParameters.displayOptions)
                        tmp.add(TapbuySDKCheckoutDisplayOption.similarProducts.rawValue)
                        self.checkoutParameters.displayOptions = tmp
                    }
                } else {
                    if self.checkoutParameters.displayOptions.contains(TapbuySDKCheckoutDisplayOption.similarProducts.rawValue) {
                        let tmp = NSMutableArray(array: self.checkoutParameters.displayOptions)
                        tmp.remove(TapbuySDKCheckoutDisplayOption.similarProducts.rawValue)
                        self.checkoutParameters.displayOptions = tmp
                    }
                }
                break
                
            default:
                break
                
            }
            
        case 5:
            
            // Other options
            
            switch indexPath.row {
                
            case 0:
                self.checkoutParameters.skipViewProduct = (updatedCell as! OnOffCell).getValue()
                break
                
            case 1:
                self.globalParameters.sandbox = (updatedCell as! OnOffCell).getValue()
                break
                
            case 2:
                self.globalParameters.debug = (updatedCell as! OnOffCell).getValue()
                break
                
            default:
                break
                
            }
            
        default:
            break
            
        }
        
    }
    
    func cellValueDeleted(updatedCell: UITableViewCell, indexPath: IndexPath) {
        
        self.checkoutParameters.voucherCodes.remove(at: indexPath.row)
        
        self.tableView.reloadData()
        
    }
    
    // MARK: Settings
    
    func settingsClicked() {
        
        // Display Save/Load menu
        
        let alert = UIAlertWrapper()
        
        alert.clickedButtonAtIndexBlock = { (buttonIndex) -> () in
            
            if buttonIndex == 1 {
                self.saveAction()
            } else if buttonIndex == 2 {
                self.loadAction()
            }
            
        }
        
        alert.presentActionSheet(
            title: "Settings",
            cancelButtonTitle: NSLocalizedString("Cancel", comment: ""),
            destructiveButtonTitle: nil,
            otherButtonTitles: [
                "Save",
                "Load"
            ],
            frame: CGRect.zero
        )
        
    }
    
    func saveAction() {
        
        // Ask user a name to associate to the saved settings
        
        let textAlert = UIAlertWrapper()
        textAlert.withTextfield = true
        textAlert.textfieldValue = self.loadedSettingsKey
        
        textAlert.clickedButtonAtIndexBlockForTexfield = { (buttonIndex, textValue) -> () in
            
            guard
                let value = textValue, value.length > 0 && buttonIndex == 1
            else { return }
            
            Helper.saveTNDParameters(key: value, productURL: self.productURL, globalParameters: self.globalParameters, checkoutParameters: self.checkoutParameters)
            
            self.loadedSettingsKey = textValue
            
        }
        
        textAlert.presentAlert(
            title: "Save settings as",
            message: "",
            cancelButtonTitle: "Cancel",
            otherButtonTitles: ["Ok"]
        )
        
    }
    
    func loadAction() {
        
        // Display settings loading screen
        
        guard
            let ctrl = self.storyboard?.instantiateViewController(withIdentifier: "testAndDebugLoadSettingsViewCtrl") as? TestAndDebugLoadSettingsViewCtrl,
            let navCtrl = self.navigationController
        else { return }
        
        ctrl.delegate = self
        
        navCtrl.pushViewController(ctrl, animated: true)
        
    }
    
    // MARK: TestAndDebugLoadSettingsDelegate
    
    func load(key: String) {
        
        if let data = Helper.loadTNDParameters(key: key) {
            
            if let tmp = data["productURL"] as? URL {
                self.productURL = tmp
            }
            
            if let tmp = data["globalParameters"] as? TapbuySDKGlobalParameters {
                self.globalParameters = tmp
            }
            
            if let tmp = data["checkoutParameters"] as? TapbuySDKCheckoutParameters {
                self.checkoutParameters = tmp
            }
            
            self.loadedSettingsKey = key
            
        }
        
    }
    
}
