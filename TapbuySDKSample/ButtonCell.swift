// Copyright (c) 2016. Tapbuy (http://tapbuy.io)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import Foundation
import UIKit

protocol ButtonCellDelegate {
    
    func buttonClicked(button: UIButton)
    
}

class ButtonCell: UITableViewCell {
    
    @IBOutlet weak var button: UIButton!
    
    var delegate: ButtonCellDelegate?
    
    func setData(buttonText: String, delegate: ButtonCellDelegate? = nil) {
        
        self.button.setTitle(buttonText, for: .normal)
        
        self.delegate = delegate
        
        button.backgroundColor = Helper.getTapbuyButtonColor()
        button.layer.cornerRadius = 5
        button.tintColor = UIColor.white
        
    }
    
    @IBAction func buttonClicked(_ sender: AnyObject) {
        
        self.delegate?.buttonClicked(button: self.button)
        
    }
    
}
