// Copyright (c) 2016. Tapbuy (http://tapbuy.io)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import Foundation
import UIKit

extension UIApplication {
    
    // Get current displayed view controller
    class func topViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        var baseController = base
        
        if baseController == nil {
            
            let windows = UIApplication.shared.windows
            
            if windows.count > 0 {
                baseController = windows[0].rootViewController
            }
            
        }
        
        if let nav = baseController as? UINavigationController {
            
            return topViewController(nav.visibleViewController)
            
        }
        
        if let tab = baseController as? UITabBarController {
            
            let moreNavigationController = tab.moreNavigationController
            
            if let top = moreNavigationController.topViewController , top.view.window != nil {
                return topViewController(top)
            } else if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
            
        }
        
        if let presented = baseController?.presentedViewController {
            
            return topViewController(presented)
            
        }
        
        return base
        
    }
    
}
